# Simple CLI and Python client for https://gitlab.com/beliav/go-push-store

## Install
For install `pip install git+https://gitlab.com/beliav/pscli.git`

## Use CLI

`pscli COMMAND [PARAMETERS]`

For view help information run `pscli` without any COMMAND



## Use Python client


For sync application use PSClient

```python
from pscli import PSClient
cl = PSClient('43.102.127.24:8081')
cl.send_value('temp', 25.7, tag='kitchen')
cl.send_event('door_open')
```

For async application use AsyncPSClient

```python
from pscli import AsyncPSClient
cl = AsyncPSClient('43.102.127.24:8081')
cl.send_value('temp', 25.7, tag='kitchen') # non blocking
cl.send_event('door_open') # non blocking

cl.wait_all() # block until all requests done
```
