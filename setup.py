import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pscli",
    version="0.1",
    author="Belyi Andrei",
    author_email="beliy.av@outlook.com",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/beliav/pscli",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=[
        'fire==0.4.0',
        'requests==2.25.1'
    ],
    entry_points={
        'console_scripts': [
            'pscli=pscli.cmd:main'
        ]
    }
)
