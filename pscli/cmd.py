import os
import sys

import requests
from requests import exceptions as excs

HOST = os.getenv('PS_HOST')


class CliHandler:
    @staticmethod
    def __print_table(data, column_names=None):
        """ Pretty print a list of dictionaries (data) as a dynamically sized table.
        If column names (colList) aren't specified, they will show in random order.
        Author: Thierry Husson - Use it as you want but don't blame me.
        """
        if not isinstance(data, (tuple, list)):
            data = [data]

        if not column_names:
            column_names = list(data[0].keys() if data else [])

        result = [column_names]  # 1st row = header

        for item in data:
            result.append([str(item[col] if item[col] is not None else '') for col in column_names])

        col_size = [max(map(len, col)) for col in zip(*result)]
        result_str = ' | '.join(["{{:<{}}}".format(i) for i in col_size])
        result.insert(1, ['-' * i for i in col_size])  # Seperating line

        for item in result:
            print(result_str.format(*item))

    @classmethod
    def _return_error(self, error):
        print(f"\033[91m{error}\033[0m")
        sys.exit(1)

    def _request(self, method, path, data=None, auth=None):
        url = f'http://{HOST}{path}'
        try:
            response = requests.request(method, url, json=data, auth=auth)
            if response.status_code != 422:
                response.raise_for_status()
        except (excs.HTTPError, excs.ConnectionError) as exc:
            self._return_error(exc)

        json_resp = response.json()
        if not json_resp['success']:
            error = f"Validation error: {json_resp['result']}"
            self._return_error(error)

        return json_resp['result']

    def create(self, uuid, name=None):
        """
        Create new metric
        """
        data = {'uuid': uuid}
        if name:
            data['name'] = name

        result = self._request('POST', '/metrics', data=data)
        return self.__print_table(result)

    def delete(self, uuid):
        """
        Delete metric
        """
        self._request('DELETE', f'/metric/{uuid}')

    def truncate(self, uuid):
        """
        Truncate all metric values
        """
        result = self._request('POST', f'/metric/{uuid}/truncate')
        return self.__print_table(result)

    def toggle(self, uuid):
        """
        Toggle metric status: ENABLED <-> DISABLED
        """
        result = self._request('POST', f'/metric/{uuid}/toggle')
        return self.__print_table(result)

    def show(self, uuid):
        """
        Show one metric by uuid
        """
        result = self._request('GET', f'/metric/{uuid}')
        return self.__print_table(result)

    def list(self, username=None, password=None):
        """
        List all metrics (need auth)
        """
        if not username:
            username = os.getenv('USERNAME')
        if not password:
            password = os.getenv('PASSWORD')

        if not username or not password:
            self._return_error('Private command! Need username and password!')

        result = self._request('GET', '/metrics', auth=(username, password))
        return self.__print_table(result)


def main():
    if not HOST:
        CliHandler._return_error('Need PS_HOST env')

    import fire
    fire.Fire(CliHandler)


if __name__ == "__main__":
    main()
