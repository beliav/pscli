import time
import logging
import weakref
from concurrent.futures import ThreadPoolExecutor, wait

import requests

LOG = logging.getLogger(__name__)


class PSClient:
    def __init__(self, host, logger=None):
        self.host = host
        self.logger = logger or LOG

    def _get_now_ts(self):
        return int(time.time())

    def _send_value(self, uuid, value, tag=None, timestamp=None):
        url = f'http://{self.host}/metric/{uuid}'
        data = {
            'value': value,
            'timestamp': timestamp or self._get_now_ts()
        }
        if tag:
            data['tag'] = str(tag)

        response = requests.post(url, json=data)
        self.logger.debug(
            'Sent value %s to metric "%s" (tag=%s, timestamp=%s). Response: %s',
            value, uuid, tag, data['timestamp'], response.status_code
        )

    def send_value(self, uuid, value, tag=None, timestamp=None):
        self._send_value(uuid, value, tag=tag, timestamp=timestamp)

    def send_event(self, uuid, tag=None, timestamp=None):
        self.send_value(uuid, 1, tag=tag, timestamp=timestamp)


class AsyncPSClient(PSClient):
    DEFAULT_WORKERS_COUNT = 5

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._futures = weakref.WeakSet()
        self._executor = ThreadPoolExecutor(max_workers=kwargs.get('max_workers', self.DEFAULT_WORKERS_COUNT))

    def send_value(self, uuid, value, tag=None, timestamp=None):

        def _future_clb(f):
            try:
                result = f.result()
            except Exception as exc:
                self.logger.exception('Exception while sending metric %s', uuid)

        f = self._executor.submit(self._send_value, uuid, value, tag=tag, timestamp=timestamp)
        f.add_done_callback(_future_clb)
        self._futures.add(f)

    def wait_all(self):
        wait(self._futures)

class DummyClient():
    def __init__(self, *args, **kwargs):
        pass

    def send_value(self, *args, **kwargs):
        pass

    def send_event(self, *args, **kwargs):
        pass

    def wait_all(self):
        pass
